package com.anshu.model;

import java.util.List;

public class Employee {
    protected int id;
    protected int managerId;
    protected String empName;
    protected List<Employee> reportees;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

    public String getName() {
        return empName;
    }

    public void setName(String name) {
        this.empName = name;
    }

    public List<Employee> getReportees() {
        return reportees;
    }

    public void setReportees(List<Employee> reportees) {
        this.reportees = reportees;
    }

    public Employee(String name,  String id, String managerId) {
        this.id = Integer.parseInt(id);
        this.empName = name;
        this.managerId = Integer.parseInt(managerId);
    }
}
