package com.anshu;

import com.anshu.model.Employee;
import com.anshu.service.TableReader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* Assumption is that we have only one CEO or single head of the organization
* We have set CEO as the employee who has no Manager ID in the table data
* We are using CSV file to represent Tabular data, eg - Anshu,100,150
* We are reading CSV file from classpath
* */
public class HierarchyRepresentation {
    public static void main(String[] args) {
        TableReader readerService = new TableReader();
        Map<Integer,Employee> emp = readerService.readTableData("EmployeeTable.csv");
        HierarchyRepresentation hr = new HierarchyRepresentation();
        Employee head = hr.findCEO(emp);
        hr.buildRanking(head,emp);
        hr.print(head,0);
    }
    public Employee findCEO(Map<Integer,Employee> emp){
        Employee ceo=null;
        for(Employee employee : emp.values()){
            if(employee.getManagerId() == 0){
                ceo = employee;
                break;
            }
        }
        return ceo;
    }
    public void buildRanking(Employee head,Map<Integer,Employee> employeeMap){
        List<Employee> reportees = getReportees(head.getId(),employeeMap);
        head.setReportees(reportees);
        if(reportees.size() == 0)
            return;
        for(Employee reportee : reportees)
            buildRanking(reportee,employeeMap);

    }
    public List<Employee> getReportees(int mgrId,Map<Integer,Employee> employeeMap){
        List<Employee> reportees = new ArrayList<Employee>();
        for (Employee em : employeeMap.values()) {
            if (em.getManagerId() == mgrId)
                reportees.add(em);
        }
        return reportees;
    }
    public void print(Employee head, int level) {
        for (int i = 0; i < level; i++)
            System.out.print("\t");
        System.out.println(head.getName());
        List<Employee> reportees = head.getReportees();
        for (Employee emp : reportees)
            print(emp, level+1);
    }
}
