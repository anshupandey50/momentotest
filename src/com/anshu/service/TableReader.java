package com.anshu.service;

import com.anshu.model.Employee;

import java.io.*;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class TableReader {
    public  Map<Integer,Employee> readTableData(String dbFile){

        Map<Integer,Employee> empMap = null;
        File file = null;
        try {
            file = getFile(dbFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(file));) {
            String input = "";
            Employee emp = null;
            empMap = new HashMap<>();
            while((input = reader.readLine()) != null){
                String[] line = input.split(",");
                emp = mapToEmployee(line);
                if(emp!=null){
                  empMap.put(emp.getId(),emp);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return empMap;
    }
    private Employee mapToEmployee(String[] emp){
        Employee employee = null;
        if(emp.length == 3 && !emp[0].equals("") && !emp[1].equals("") && !emp[2].equals(""))
            employee = new Employee(emp[0],emp[1],emp[2]);
        else if(emp.length == 2 && !emp[0].equals("") && !emp[1].equals(""))
            employee = new Employee(emp[0],emp[1],"0");

        return employee;
    }
    private File getFile(String fileName) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile());
        }
    }
}
